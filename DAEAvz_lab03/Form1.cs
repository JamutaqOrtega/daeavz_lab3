﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DAEAvz_lab03
{
    public partial class Form1 : Form
    {
        SqlConnection conn; //permite manejar el acceso al servidor

        public Form1()
        {
            InitializeComponent();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            string servidor = txtServidor.Text;
            string bd = txtBaseDatos.Text;
            string user = txtUsuario.Text;
            string pwd = txtPassword.Text;

            string str = $"Server={servidor};DataBase={bd};";

            //la cadena de conexion cambia en funcion del checkbox
            if (chkAutenticacion.Checked)
                str += "Integrated Security=true";
            else
                str += $"User Id={user};Password={pwd};";

            //abre una conexion con el servidor usando la cadena de c.
            try
            {
                conn = new SqlConnection(str);
                conn.Open();
                MessageBox.Show("Conectado satisfactoriamente");
                btnDesconectar.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al conectar el servidor: \n" + ex.Message);
            }
        }

        private void btnEstado_Click(object sender, EventArgs e)
        {
            //intenta obtener el estado de la conexcion y si esta abierda
            //recupera la info de la misma
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    MessageBox.Show($"Estado del servidor: {conn.State}" +
                        $"\nVersión del servidor: {conn.ServerVersion}" +
                        $"\nBase de datos: {conn.Database}");
                }
                else
                    MessageBox.Show($"Estado del servidor {conn.State}");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Imposible determinar el estado del servidor: \n" + ex.Message);
            }
        }

        private void btnDesconectar_Click(object sender, EventArgs e)
        {
            //para cerrar la conexion, se debe verificar que no está cerrada
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Close();
                    MessageBox.Show("Conexión cerrada");
                }
                else
                    MessageBox.Show("La conexión ya está cerrada");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al cerrar la conexión: \n" + ex.Message);
            }
        }

        private void chkAutenticacion_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAutenticacion.Checked)
            {
                txtUsuario.Enabled = false;
                txtPassword.Enabled = false;
            }
            else
            {
                txtUsuario.Enabled = true;
                txtPassword.Enabled = true;
            }
        }

        private void btnPersona_Click(object sender, EventArgs e)
        {
            Persona persona = new Persona(conn);
            persona.Show();
        }
    }
}
